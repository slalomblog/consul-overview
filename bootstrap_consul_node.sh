#!/bin/bash 

if [ "$EUID" -ne 0 ]
  then
      echo "You must be root to install consul"
  exit
fi

function usage {
    echo "Usage: sudo $0"
}

## CONSTANTS ##

# /bin should be for system bins, and let's make it available for all users
INSTALL_DIR=/usr/bin
# Make output from this script more obvious
NONE='\033[00m'
BOLD='\033[1m'

##############

## HELPERS ###
function pretty {
    echo -e "$BOLD$1$NONE"
    tput sgr0
}
##############

if [ $# -gt 0 ] 
then
    usage
    exit 1
fi

pretty "Preparing for Consul install..."

pushd ~ > /dev/null

which unzip > /dev/null
if [ $? -ne 0 ]
then
    pretty "Can't find package unzip. Installing..."
    pretty "Updating Apt database..."
    apt-get update > /dev/null
    pretty "Apt updated."
    apt-get -y install unzip > /dev/null
    if [ $? -ne 0 ] 
    then
        pretty "Failed to install unzip - needed for consul install. Aborting"
        exit 2
    fi   
fi

pretty "Retrieving consul binary...."
wget https://dl.bintray.com/mitchellh/consul/0.5.2_linux_amd64.zip

pretty "Unzipping archive"
unzip 0.5.2_linux_amd64.zip

pretty "Copying consul to $INSTALL_DIR"
cp -r consul $INSTALL_DIR

popd > /dev/null

pretty "Done"
